% !TEX root = ./chuan-ipm-eal.tex

\section{Multi-Interaction based Convolutional Matching Network}
\label{section_method}

In this section we introduce a novel neural matching network specifically designed for entity aspect linking. As discussed in introduction, our model, MICMN (Multiple Interaction based Convolutional Matching Network) consists of four layers. It first accepts textual sequence as inputs. Then, given their input embeddings, it models four kinds of interactions as matching patterns from different perspectives. Furthermore, these interactions are passed through convolutional layers and a novel pooling strategy is used to extract local features. Finally, a fusion layer is used to combine all matching signals to produce a score for ranking. Our overall model architecture is shown in Figure~\ref{fig_model} and each of the above layers is described in detail below.

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{z.figures/sec-3-model-architecture.eps}
%\vspace{5cm}
\caption{Overview of the Multi-Interaction Convolutional Matching Network.}
\label{fig_model}
\end{figure}

\subsection{Input}
\label{section_input}

% Describe the embedding layer and the output of embedding layer, i.e. a matrix.
The inputs to our model are an entity context $q$ and a candidate aspect document $c$, which consists of context terms $\{w_1^q,w_2^q,\ldots,w_m^q\}$ and document terms $\{w_1^c,w_2^c,\ldots,w_n^c\}$ respectively. $m$ and $n$ are the number of terms in $q$ and $c$. We employ an embedding layer to convert each term into a $L$-dimensional vector representation, generating matrix representation $Q\in R^{m\times L}$ and $C\in D^{n\times L}$ for $q$ and $c$ respectively.

\subsection{Interactions from multiple perspective}
\label{section_interactions}

Given $Q$ and $C$, we construct an interaction matrix $M$, where each element $M_{ij}$ stands for the basic interaction, i.e., similarity between word $w_i^q$ and $w_j^c$ (see Equation~\ref{eq:1}) and the operator $\otimes$ stands for an operator to obtain the similarity.
\begin{equation}
M_{ij}=w_i^q \otimes w_j^c .  \label{eq:1}
\end{equation}
The way interaction matrix is built is similar with that in~\cite{pang2016text}. We adopt different kinds of $\otimes$ operator to model the interactions between two words. 

\subsubsection{Exact match interaction}
The first operator is the \textit{\textbf{indicator function}}, which accepts token inputs and produces 1 to indicate that two words are identical and 0 otherwise. The element at $i$-th row and $j$-th column in exact match interaction matrix $M^{em}$ is shown in Equation~\ref{eq_indicator}:
\begin{equation}
M_{ij}^{em}=I_{w_i^q=w_j^c}=\left\{
             \begin{array}{ll}
          1,  & \mathrm{if~}w_i^q=w_j^c   \\
          0,  & \mathrm{otherwise}. 
             \end{array}
             \label{eq_indicator}
\right.
\end{equation}
This is similar to the indicator matching matrix used in~\cite{pang2016text,mitra2017learning}. The indicator function can capture exact match signals, which has proved to be a good baseline for entity aspect linking, as shown by the content overlap method in~\cite{nanni2018entity}. However, one limitation of the indicator function is that it cannot capture matching signals between two semantically similar words. To address this issue, we resort to soft match interaction, self-attentive interaction and self-attention weighted soft match, which are capable of capturing semantically similar words.

\subsubsection{Soft match interaction}

Soft match interaction is used in various semantic matching tasks, such as paraphrase identification~\cite{pang2016text}, ad-hoc retrieval~\cite{xiong2017end,dai2018convolutional}, and short text matching~\cite{chen2018mix}. In our model, we use \textbf{\textit{cosine similarity}} as the interaction operator and compute the semantic similarity between words using pre-trained embeddings.  The embedding of word $w_i^q$ and $w_j^c$ are denoted as $v_i$ and $v_j$ respectively. The element at $i$-th row and $j$-th column in soft match interaction matrix $M^{sm}$ is shown in Equation~\ref{eq_sm}; it may be viewed as a soft indicator function:
\begin{equation}
M_{ij}^{sm}=\frac{v_i v_j}{\| v_i\| \ldots \| v_j \|}.
\label{eq_sm}
\end{equation}

\subsubsection{Self-attentive interaction}

The third type of interaction we consider is self-attentive interaction, which applies self attention over $q$ and $c$ respectively and then combines the weights together. Specifically, it takes as an input the word embeddings of the sequence $q$, i.e., $Q$, and outputs a vector of self-attentive weights $a_q$:
\begin{equation}
a_q=\mathrm{softmax}(w_{s2} \tanh(W_{s1} Q^T)).
\end{equation}
Here, $W_{s1}$ is a weight matrix with a shape of $u\times L$, and $w_{s2}$ is a vector of parameters with size $u$, where $u$ is a hyperparameter we can set arbitrarily. Since $Q$ is a matrix of $m\times L$, the weight vector $a_q$ will have size $m$. The $\mathrm{softmax}(\cdot)$ ensures that all the computed weights sum up to 1. The weights of the sequence $c$, i.e., $a_c$, can be computed in the same way. The size of $a_c$ is $n$.

After obtaining the weights of input sequences, i.e. $a_q$ and $a_c$, we compute the self-attentive weights for soft match interaction $W_{sa}$ as follows:
\begin{equation}
W^{sa}=a_q(a_c)^T.
\label{eq_sa}
\end{equation}
The size of $W^{sa}$ is $m\times n$, since $a_q$ and $a_c$ are vectors of size $m$ and $n$ respectively. Then we apply self attention weights to soft match interaction as follows:
\begin{equation}
M^{sw}=M^{sm} \cdot W^{sa}.
\label{eq_sw}
\end{equation}

\subsection{Convolutional matching}
\label{section_convolution}

Given the aforementioned multi-perspective interactions, we apply a separate convolutional layer to each interaction matrix to extract convolved features. Since the operation over all interactions is similar, we take $M^{sm}$ as an example. Given $M^{sm}$, the convolutional layer applies convolutional filters to the matrix to capture the compositional matching patterns of consecutive terms. $F$ different filters with square kernel are used to extract features, each describing the n-gram local matching in a different perspective. The kernel size is set to $k$. Then we add a bias and apply a non-linear activation function and obtain convolved features $CF$ for each interaction. 

% ############################
% Put the figure so that the figure is on the same page as section{Q-singular pooling}.
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{z.figures/fig-section-3-q-singular-pooling.eps}
\caption{Illustration of q-singular pooling.}
\label{fig_q_singular_pooling}
\end{figure}
% ############################

\subsection{Q-singular pooling}

% Describe the column wise pooling operation used in~\cite{santos2016attentive,zhang2017attentive}.
Given the convolved features, we want to extract features using the pooling operation. Column-wise max-pooling is proposed in~\cite{santos2016attentive}, which operates over $M$ to generate the vectors $g_q\in R^m$ and $g_c\in R^n$ respectively. Formally, the $j$-th element of $g_q$ and $g_c$ are computed as follows:
\begin{eqnarray}
{}[g^q]_j&=&\max \limits_{1<u<m}[G_{j,u}] \\
{}[g^c]_j&=&\max \limits_{1<v<n}[G_{v,j}].
\end{eqnarray}
The $j$-th element of vector $g_c$ can be viewed as an importance score for the context around the $j$-th word in the candidate aspect $c$ with respect to the entity context $q$. Similarly, the $j$-th element of vector $g_q$ can be viewed as the importance score for the context around the $j$-th word in $q$ with regard to $c$. This operation is adopted in~\cite{zhang2017attentive} and applied over both rows and columns over 3D tensors.

% Describe our operation
Inspired by the row-wise and column-wise pooling in~\cite{santos2016attentive}, we propose \emph{q-singular max-sum pooling} (q-singular pooling) over convolved features. Specifically, given convolved feature matrix $G$, we first perform row-wise max pooling over $G$ to get a scalar for each row. And then, scalars in each row are summed up as the final feature value. The q-singular pooling operation applied over two convolved feature matrices are illustrated in Figure~\ref{fig_q_singular_pooling}.

As an example, consider the first case where most entity context words are matched in candidate aspect, as shown in Figure~\ref{fig_q_singular_pooling} (a). In contrast, the second case is shown in Figure~\ref{fig_q_singular_pooling} (b), where one word in the entity context is matched in candidate aspect multiple times. Though the number of exact match counts is the same in both cases, we consider the first case as better matching between entity context and candidate aspect. Formally, the q-singular max-sum pooling is computed as follows:
%
\begin{equation}
\mbox{qs-pooling}(G)=\sum \limits_{[1,m]}{\max \limits_{j\in [1,n]}[G_{j}]}.
\end{equation}
%
Q-singular pooling is applied to each convolved feature matrix and a scalar feature value is used.

\subsection{Score}
\label{section_fusion}

The convolutional matching and pooling operations make a convolutional block and the number of blocks used for each interaction is $B$. The feature vectors produced by convolutional blocks are concatenated to one feature vector $h$. We use one full-connected layers to produce the final matching score, which is obtained by Equation~\ref{eq:score}:
\begin{equation}
score = h^T W + b \label{eq:score}.
\end{equation}
where $W$ and $b$ are the weight and bias. The matching score is activated by a $\tanh$ activation function such that the matching score is between $-1$ and $1$.

We adopt a pair-wise learning to rank objective to learn the parameters for predicting the final matching score:
\begin{equation}
L= \sum_{q,c+,c-}\max((1-score(q,c+)+score(q,c-)),0).
\end{equation}