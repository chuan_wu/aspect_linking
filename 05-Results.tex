% !TEX root = ./chuan-ipm-eal.tex

\section{Results}
\label{section_results}

\subsection{Quantitative results}

% Present the table; discussion on simple baselines
The quantitative results on entity aspect linking are shown in Table~\ref{table-results-1}. Each row of the table represents a method for entity aspect linking. As shown by results over EAL18 in Table~\ref{table-results-1}, MICMN is significantly better than neural baselines,\footnote{We test for statistical significance using a paired t-test.  All significance tests are performed at the $\alpha=.05$ level.} while worse than content overlap and IR baselines on small datasets. Since the EAL18 dataset has only 201 instances, it shows that the IR baselines are the best choice when there is little training data.

On the other hand, when we turn to dataset with a large size, content overlap is the best choice. As we can see, content overlap achieves the best performing results on EAL-DO and EAL-DS, and is also competitive with tf-idf on EAL-DB. Our model is competitive with IR baselines and content overlap. The reason why we are not performing better might result from the fact that we are using limited information. We restrict the maximum length of the entity context and candidate aspect to 30 and 300 respectively for neural models. To show the impact of this restriction, we apply this restriction to content overlap and give the results indicated by \textit{content overlap (lim)}. As shown by the third and fourth row, the limitation leads to a drop in terms of both P@1 and MAP, and the results are not as good as our model. The difference is significant on all datasets except EAL-DB. We consider it as an implication that when entity context and candidate aspects are not limited in length, MICMN is likely to perform better than naive baselines and IR baselines.

The performance of neural baselines is not as good as naive baselines and IR baselines, showing that existing neural ranking models are not naturally adaptable to entity aspect linking. The performance of MICMN is consistently better than neural baselines, which shows the superiority of MICMN for entity aspect linking.

\begin{table*}[t]
\centering
\caption{Entity aspect linking results on four datasets.}
\label{table-results-1}
\begin{tabular}{l cc cc cc cc}
\toprule
\multirow{2}{*}{Method}    &    \multicolumn{2}{c}{EAL18}   &   \multicolumn{2}{c}{EAL-DB}   &   \multicolumn{2}{c}{EAL-DO}   &   \multicolumn{2}{c}{EAL-DS}  \\
\cmidrule(r){2-3}
\cmidrule(r){4-5}
\cmidrule(r){6-7}
\cmidrule{8-9}
                    &  \textbf{P@1}  &   \textbf{MAP}   &  \textbf{P@1}  &   \textbf{MAP}   &  \textbf{P@1}  &   \textbf{MAP}   &  \textbf{P@1}  &   \textbf{MAP}    \\
\midrule
random   & 21.89 & 45.32    & 17.13 & 39.62 & 17.02 & 39.88 & 18.52 & 41.70  \\
\midrule
size         & 40.40 & 61.14 & 44.42 & 61.50 & 43.91 & 62.65 & 43.47 & 62.33  \\
content overlap & 56.22 & 72.15 & 53.98 & 69.21 & \textbf{52.75} & \textbf{69.26} & \textbf{55.77} & \textbf{71.50}  \\
\midrule
tf-idf  &  \textbf{59.70}     &  \textbf{74.52}    &  \textbf{56.89}     &  \textbf{70.76}    &  47.56     &   64.82    &  53.51     &   68.79   \\
BM25  &  56.22    &  71.76    &  54.45     &   69.05    &  50.99     &   66.25    &  50.00     &   66.39   \\
\midrule
\textit{content overlap (lim)} & 57.21 & 72.71 & 48.73 & 65.53 & 43.41 & 63.57 & 50.49 & 67.88  \\
\midrule
K-NRM  &  27.82  &  50.18    &  24.74  &  46.19  &  34.07  &  54.57  &  26.90  &  49.31  \\
Conv-KNRM  &  32.00  & 54.27  &  44.17  &  60.78  &  44.52  &  62.16  &  44.50  & 62.53  \\
\midrule
MICMN  & 49.28 & 68.01 &  56.32  & 70.42  &  50.48  &  66.37  &  53.69  &  69.21    \\
\bottomrule
\end{tabular}
\end{table*}

\subsection{Ablation study}

An ablation study is performed to better understand the contribution of each module in our proposed model. By removing one component at a time from the full system and performing re-training and re-testing, we are able to study the effectiveness of each module. Here, we first study how exact match, soft match and self-attentive soft match contribute to the effectiveness of the model. Results on each dataset are shown in Table~\ref{table-results-ablation-interaction}, with each row denoting the removal of a particular module. For example, the row ``-- exact match'' represents removing the exact match module.

\begin{table*}[t]
\centering
\caption{Results of MICMN with different components removed.}
\label{table-results-ablation-interaction}
\begin{tabular}{l cc cc cc cc}
\toprule
\multirow{2}{*}{Method}    &    \multicolumn{2}{c}{EAL18}   &   \multicolumn{2}{c}{EAL-DB}   &   \multicolumn{2}{c}{EAL-DO}   &   \multicolumn{2}{c}{EAL-DS}  \\
\cmidrule(r){2-3}
\cmidrule(r){4-5}
\cmidrule(r){6-7}
\cmidrule{8-9}
                    &  \textbf{P@1}  &   \textbf{MAP}   &  \textbf{P@1}  &   \textbf{MAP}   &  \textbf{P@1}  &   \textbf{MAP}   &  \textbf{P@1}  &   \textbf{MAP}    \\
\midrule
MICMN  & \textbf{49.28} & 68.01 &  56.32  & 70.42  &  50.48  &  66.37  &  53.69  &  69.21    \\
\midrule
-- exact match    & 33.82 & 56.39    & 43.86 & 61.20 & 48.04 & 65.03 & 50.32 & 66.98  \\
-- soft match  & \textbf{49.28} & \textbf{68.32}    & 56.61 & 70.50 & \textbf{51.50} & \textbf{67.46} & \textbf{54.81} & \textbf{69.84}  \\
-- self-attentive soft match  & 46.29 & 66.16 &  \textbf{57.26}  & \textbf{70.77}  &  50.37 & 66.54 & 54.56 & 69.78  \\
\bottomrule
\end{tabular}
\end{table*}

% First part of the results. Talk about the performance between
From the first two rows ``-- exact match'', we can see that removing exact match leads to a significant effectiveness drop. This confirms that exact match is always an important indicator of relevance in semantic matching tasks. When soft match component is removed, the results is better than that the case of self-attentive soft match being removed. This shows that weighted soft match can help improve the performance compared to naive soft match.

\begin{table*}
\centering
\caption{Results of MICMN with different pooling strategies.}
\label{table-results-ablation-pooling}
\begin{tabular}{l cc cc cc cc}
\toprule
\multirow{2}{*}{Method}    &    \multicolumn{2}{c}{EAL18}   &   \multicolumn{2}{c}{EAL-DB}   &   \multicolumn{2}{c}{EAL-DO}   &   \multicolumn{2}{c}{EAL-DS}  \\
\cmidrule(r){2-3}
\cmidrule(r){4-5}
\cmidrule(r){6-7}
\cmidrule{8-9}
                    &  \textbf{P@1}  &   \textbf{MAP}   &  \textbf{P@1}  &   \textbf{MAP}   &  \textbf{P@1}  &   \textbf{MAP}   &  \textbf{P@1}  &   \textbf{MAP}    \\
\midrule
q-singular pooling  & \textbf{49.28} & \textbf{68.01} &  \textbf{56.32}  & \textbf{70.42}  &  \textbf{50.48}  &  \textbf{66.37}  &  \textbf{53.69}  &  \textbf{69.21}    \\
max pooling    & 42.82 & 62.55    & 53.32 & 67.14 & 46.60 & 65.03 & 49.30 & 65.74  \\
mean pooling  & 40.84 & 61.81  & 51.73  & 66.41  & 48.02  &  64.76 &  49.76  &  65.94  \\
\bottomrule
\end{tabular}
\end{table*}

After studying the effectiveness of different interactions, we compare the proposed q-singular pooling with existing pooling methods. Results are shown in Table~\ref{table-results-ablation-pooling}. Here we compare with max pooling and mean pooling. Comparing the first and the last column, we can see large drops when using max pooling compared to that of Q-singular pooling, indicating that Q-singular pooling is a better pooling strategy for entity aspect linking.

\subsection{Parameter analysis}

\begin{figure}
\centering
\includegraphics[clip,trim=45mm 0mm 45mm 0mm,width=1.0\textwidth]{z.figures/sec-5-param-analysis.eps}
%\vspace{5cm}
\caption{MAP scores with different maximum number of training epochs and maximum length of entity context and candidate aspect.}
\label{fig_param_analysis}
\end{figure}

\textbf{Results with different number of epochs.} We vary the number of maximum training epochs $max\_epochs$ from 5 to 20 and report the results on the four datasets in Figure~\ref{fig_param_analysis} (a). With the increase of $max\_epochs$, some improvements can be observed on eal18, and not much difference is observed with other datasets. Actually, the performance becomes stable for bigger dataset when $max\_epochs$ is above 10. The results show that our model might converge after training for around 10 epochs.

\textbf{Results with different maximum entity context length.} We also explore the impact of the maximum length of entity contexts and candidate aspects. We first vary the maximum entity context length from 10 to 30 and fix candidate aspect length to 300. The result is shown in Figure~\ref{fig_param_analysis} (b). The figure shows that longer entity contexts slightly improve the performance. As we can see from Table~\ref{table-dataset-stats}, the length of most entity contexts is around 15 and covering instances with longer entity contexts is not that helpful for the overall performance.

\textbf{Results with different maximum aspect length.} Next we vary the maximum candidate aspect length from 100 to 300 and fix the maximum entity context length to 30. The result is shown in Figure~\ref{fig_param_analysis} (c). The results show that with the increase in candidate aspect length, the overall performance increases. This shows that longer candidate aspects are helpful. However, we also observe that the performance improvements are getting smaller with the increase in aspect length. Since increasing aspect length means a higher computational cost, in a practical setting the costs and benefits need to be weighted carefully.