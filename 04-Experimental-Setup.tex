% !TEX root = ./chuan-ipm-eal.tex

\section{Experimental Setup}
\label{section_experimental_setup}

In the remainder of the paper we address the following research questions: (RQ1) How does MICMN compare with state-of-the-art approaches for entity aspect linking? (RQ2) How do different components of MICMN affects the performance? (RQ3) What is the impact of parameters on MICMN?

\subsection{Dataset}

% Describe the first dataset and the other three datasets
We evaluate our method using four datasets. The first dataset used in our experiments is EAL18~\cite{nanni2018entity}, which consists of 201 instances. Entity contexts are provided at three levels of granularity, i.e., sentence context, paragraph context and section context. Candidate aspects are represented by a header, a content with one or more passages and a list of entities that appears in content. The other three datasets, i.e. EAL-DB, EAL-DO and EAL-DS, are subsets of EAL19-D~\cite{nanni2019entity}. Compared with EAL18, candidate aspects is represented in a similar way with EAL18, while sentence context is the only entity context available. Statistics of the four datasets are shown in Table~\ref{table-dataset-stats}.

% Describe the major differences between different datasets.
There are two major differences between EAL18 and the other three datasets. First, in addition to sentence context, there are also paragraph context and section context available in EAL18. Second, entity annotations are available for entity context in EAL18, while not available in EAL-DB, EAL-DO and EAL-DS. To keep the consistency of our experiments across datasets, we use the sentence context only as our entity context. Besides, we do not make use of entity annotations in either entity context or candidate aspects since we do not have information about entities in the entity context.

We use NLTK tokenizer\footnote{http://www.nltk.org} for preprocessing. Since only the sentence context is available in the three subsets of EAL19-D, we only use sentence context in EAL18 as the entity context. We split each dataset into 5 fold and conduct 5-fold cross validation. We use 70\% of the data for training, 10\% for validation and the remaining 20\% for evaluation.

\subsection{Baselines}

\begin{table}
\centering
\caption{The minimum/average/maximum number of candidate aspects, length of aspect content and length of entity (sentence) context.}
\begin{tabular}{l rcccc |}
\toprule
\textbf{Name}   &  \textbf{Size}  &   \textbf{Entity Context Length}  &  \textbf{\# Aspects}   &   \textbf{Aspect Length}    \\
\midrule
EAL18   &  201     &   4/15.1/68   &  2/6.3/29   &     2/334/21756  \\
EAL-DB &  1067  &   3/17.9/70   &   2/9.4/140   &     1/210.7/13487     \\
EAL-DO  &  13111  &   2/16.2/74   &   2/7.9/140   &     1/209.3/13770    \\
EAL-DS  &  8338    &   2/16.7/67   &   2/7.4/140   &     1/223.8/13770   \\
\bottomrule
\label{table-dataset-stats}
\end{tabular}
\end{table}

In order to demonstrate the effectiveness of our proposed model, we employ the following baselines.

\textbf{Naive baselines}. \textit{Random} ranks candidate aspects randomly. \textit{Size} ranks aspects by the length of aspects. \textit{Content overlap} ranks aspects by the number of overlapping tokens in $q$ and $c$.

\textbf{IR baselines}. \textit{tf-idf} and \textit{BM25} are traditional information retrieval baselines.

\textbf{Neural baselines}. \textit{K-NRM}~\cite{xiong2017end} uses a translation matrix that models word-level similarities via word embeddings, a kernel-pooling technique that uses kernels to extract multi-level soft match features, and a learning-to-rank layer that combines those features into the final ranking score. \textit{Conv-KNRM}~\cite{dai2018convolutional} uses convolutional neural networks to represent $q$ and $c$, performs soft matches between them and utlize kernel pooling and learning-to-rank layers for producing the final ranking score. Conv-KNRM is a state-of-the-art approach.

\subsection{Evaluation metrics and parameter setting}

% Evaluation metrics. We use a particular metric; explain a bit about the metric.
Following~\cite{nanni2018entity}, we employ Precision@1(P@1) and Mean Average Precision (MAP) to evaluate the experimental results. P@1 is the average number of times that the best answer is ranked 1st among all candidate aspects. MAP is average of the average precision value for a set of queries.

To enable fair comparisons with the baselines, we adopt the same training setup in all experiments wherever possible, including embeddings, optimizer and hyper-parameters. We use the fixed Glove embeddings~\cite{pennington2014glove} with a dimension size of 50. Tokens that did not appear in the pre-trained word embeddings are replaced with a special token, of which the embedding is initialized randomly. We train all models with the Adam optimizer with an learning rate of $3\times 10^{-4}$. The L2 regularization is set to $10^{-6}$ and a dropout of $d=0.8$ is applied to all layers (except the embedding layer). The batch size is set to 64. The window size and number of feature map of CNN is 3 and 128.

The number of kernels in the kernel pooling layers in K-NRM and Conv-KNRM is 11 kernels/bins. The first one is the exact match kernel $\mu=1$, $\sigma=10^{-3}$, or bin $[1,1]$. The other 10 kernels/bins equally split the cosine range $[-1, 1]$: the $\mu$ or bin centers are: $\mu_1=0.9, \mu_2=0.7, \ldots, \mu_{10}=-0.9$. The $\sigma$ of the soft match bins were set to be 0.1~\cite{xiong2017end}. We implement our model using MatchZoo library~\cite{guo2019matchzoo} and use their implementation of K-NRM and Conv-KNRM for our experiments.

% Statistical test. We run statistical significance tests using Fisher’s two-sided, paired randomization test (Smucker, Allan, and Carterette 2007) against the three non-neural baselines: QL, RM3, and L2R (with all features), and the best neural baselines: K-NRM+ and PACRR+. Superscripts and subscripts indicate the row indexes for which a metric difference is statistically significant at $p<0.05$.